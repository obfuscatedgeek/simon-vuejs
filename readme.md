# Simon game ins Vuejs

Simon game implemented in vuejs

## Installation with server
  - clone the repo
  - run `npm install`
  - run `npm run dev`
  - open browser and navigate to `http://localhost:1337`

## Installation without server
  - clone the repo
  - browse to directory and open index.html in your favourite browser

## Customizations

### Change port
Open `server.js` in your favourite editor and change the value on `line 4`

## Screenshot

![Screenshot](https://gitlab.com/obfuscatedgeek/simon-vuejs/raw/master/demo.png)

