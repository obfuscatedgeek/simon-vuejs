const app = new Vue({
  el: '#app',
  data: {
    complexity: 4,
    spheres: [],
    computer_pattern: [],
    user_pattern: [],
    player_turn: 'ai',
    status: 'off',
    menu: 'on',
    notification: '',
    high_score: 0,
    strict: false
  },
  methods: {
    updateComplexity () {
      if (!this.complexity || this.complexity < 2) {
        this.notify('Need at least 2 sphere for the game to initialize', 3000);
        return;
      }
      this.createSpheres();
      this.resetGame();
      this.menu = 'off';
    },
    /**
     * generates the spheres based on the complexity count
     */
    createSpheres () {
      this.spheres = [];
      for (let i = 0;i<this.complexity;i++) {
        let color = Math.random().toString(16).slice(2, 8).toUpperCase();
        this.spheres.push(color);
      };
    },

    /**
     * handler for strict mode checkbox
     * @param {Object} e
     */
    toggleStrict (e) {
      this.strict = e.target.checked;
    },

    startGame () {
      if (this.status === 'on') {
        this.notify('Game is already on, click "stop game" or "reset game"');
        return;
      }
      this.player_turn = 'ai';
      this.status = 'on';
      this.startLevel();
    },

    /**
     * shows the menu
     */
    stopGame () {
      this.menu = 'on';
    },

    /**
     * reset the game with initial parameter
     */
    resetGame () {
      this.computer_pattern = [];
      this.user_pattern = [];
      this.player_turn = 'ai';
      this.status = 'off';
    },

    /**
     * begins the next level
     */
    startLevel () {
      this.generateComputerPattern();
    },

    /**
     * toggle user from user to ai and back
     */
    switchUser () {
      const current_user = this.player_turn;
      this.player_turn = current_user === 'ai' ? 'human' : 'ai';
    },

    /**
     * generates the pattern for next level
     */
    generateComputerPattern () {
      const spheres = this.spheres;
      this.computer_pattern.push(spheres[Math.floor(Math.random() * spheres.length)]);
      this.playComputerPattern();
    },

    /**
     * click handler for sphere.
     *
     * @param {String} v
     * @param {Object} e
     */
    sphereClick (v, e) {
      if (this.status === 'off') {
        this.notify("Please click 'start game' button to begin");
        return;
      }
      if (this.player_turn === 'ai') {
        this.notify("AI's turn to play");
        return;
      }
      const div = e.target;
      this.sleep(50).then(() => {
        div.classList.toggle('active');
        return this.sleep(500);
      }).then(() => {
        div.classList.toggle('active');
        return this.sleep(500);
      }).then(() => {
        this.user_pattern.push(v);
        this.verifyPattern();
      });
    },

    /**
     * verify if the user click sequence matches the AI generated pattern
     */
    verifyPattern () {
      const item_index = this.user_pattern.length -1;
      const item = this.user_pattern[item_index];

      if (this.computer_pattern[item_index] !== item) {
        if (this.strict === true) {
          this.sleep(1).then(() => {
            this.notify('Strict mode: Error results in restarting from the beginning. /n Click "start game" to begin.');
            return this.sleep(2000)
          }).then(() => {
            this.resetGame();
          });
        } else {
          this.notify('Error: Watch again', 1500);
          this.user_pattern = [];
          this.playComputerPattern(false);
        }
      } else {
        if (this.user_pattern.length === this.computer_pattern.length) {
          this.high_score = this.user_pattern.length > this.high_score ? this.user_pattern.length : this.high_score;
          this.user_pattern = [];
          this.switchUser();
          this.startLevel();
          this.notify('Pefect: Next level begins', 1000);
        }
      }
    },

    /**
     * delays
     * @param {Number} time
     */
    sleep (time) {
      return new Promise(resolve => {
        setTimeout(resolve, time)
      })
    },

    /**
     * Plays the computer pattern
     * @param {Boolean} switchUser
     */
    playComputerPattern (switchUser = true) {
      let counter = 0;
      const me = this;

      (function next() {
          const el = me.$el.querySelector('#_'+me.computer_pattern[counter]);
          if (counter++ > me.computer_pattern.length-1) {
            me.sleep(1500).then(() => {
              me.notify('Its your turn now', 1000);
              if (switchUser === true) {
                me.switchUser();
              }
            })
            return;
          };
          setTimeout(() => {
            me.sleep(100).then(() => {
              el.classList.toggle('active');
                return me.sleep(500);
              }).then(() => {
                el.classList.toggle('active');
              });
                next();
            }, 1000);
        })();
    },

    /**
     * Displays the notification messages.
     * @param {String} msg
     * @param {Number} interval
     */
    notify (msg, interval = 3000) {
      this.sleep(10).then(() => {
        this.notification = msg;
        return this.sleep(interval);
      }).then(() => {
        this.notification = '';
      });
    }
  },

  created () {
    this.createSpheres();
  }
});
